package com.example.homework1

import android.content.Context
import android.util.Log
import android.widget.Toast

class Utils {

    companion object {
        private val TAG = "myLogs"

        fun addToLog(message : String){
            Log.d(TAG, message)
        }

        fun showToast(context: Context, text:String) {
            val toast = Toast.makeText(context, text, Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}