package com.example.homework1

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView

class CustomRecyclerAdapter(private val dataSet: MutableList<RowType>, private val onButtonClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return when (dataSet[position]) {
            is RowType.ButtonRowType -> 0
            is RowType.TextRowType -> 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType)  {
            0 -> {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.custom_button, parent, false)
                ButtonViewHolder(view, onButtonClickListener)
            }
            else -> {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.text_edit_item, parent, false)
                TextViewHolder(view, dataSet)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TextViewHolder -> {
                holder.bind(dataSet[position] as RowType.TextRowType)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    class ButtonViewHolder(itemView: View, onButtonClickListener: View.OnClickListener) : RecyclerView.ViewHolder(itemView) {
        var button: Button = itemView.findViewById(R.id.button) as Button
        init {
            button.setOnClickListener(onButtonClickListener)
        }
    }

    class TextViewHolder(itemView:View, var dataSet: MutableList<RowType>):RecyclerView.ViewHolder(itemView) {
        var editTextView:EditText = itemView.findViewById(R.id.editText) as EditText

        fun bind(item: RowType.TextRowType) = this.editTextView.setText(item.editTextValue)

        init{
            editTextView.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(charSequence:CharSequence, i:Int, i1:Int, i2:Int) {
                }
                override fun onTextChanged(charSequence:CharSequence, i:Int, i1:Int, i2:Int) {
                    (dataSet[position] as RowType.TextRowType).editTextValue = editTextView.text.toString()
                }
                override fun afterTextChanged(editable: Editable) {
                }
            })
        }
    }
}
