package com.example.homework1
sealed class  RowType {

    object ButtonRowType : RowType()

    class TextRowType() : RowType() {
        var editTextValue: String? = null
    }
}