package com.example.homework1

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), AddElemDialogFragment.AddElemDialogListener {

    private lateinit var items: MutableList<RowType>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        var lastConfiguration = lastCustomNonConfigurationInstance
        items = if (lastConfiguration == null){
            mutableListOf()
        } else {
            lastConfiguration as MutableList<RowType>
        }
        floatingActionButton.setOnClickListener(onFloatingActionButtonClickListener)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CustomRecyclerAdapter(items!!, onButtonClickListener)
    }

    override fun onRetainCustomNonConfigurationInstance(): MutableList<RowType> {
        return items
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.createButton -> addElements(listOf("button"))
            R.id.createText -> addElements(listOf("text"))
            R.id.clear -> {
                items?.clear()
                recyclerView.adapter?.notifyDataSetChanged()
                Utils.addToLog("View cleaned")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private val onFloatingActionButtonClickListener = View.OnClickListener {
        val addElemDialogFragment = AddElemDialogFragment.newInstance()
        val manager = supportFragmentManager
        addElemDialogFragment.show(manager, "addElemDialog")
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, text: String) {
        val elementsList = text.replace(" ", "").split(",")
        addElements(elementsList)
    }

    private val onButtonClickListener = View.OnClickListener {
        var textEditCountOnView = 0
        var itemsList: MutableList<String> = mutableListOf()

        for (i in 0 until (items?.size!!)) {
            (items?.get(i) as? RowType.TextRowType)
                .let {
                    if (it?.editTextValue != null) {
                        textEditCountOnView++
                        itemsList.add(it.editTextValue!!)
                        it.editTextValue = ""
                    }
                }
        }
        if (textEditCountOnView == 0) {
            Utils.showToast(this, "You need at least one text input field")
        } else {
            addElements(itemsList)
        }
    }

    private fun addElements(input: List<String>){
        var buttonCount = 0
        var textEditCount = 0
        for(elem in input){
            when(elem){
                "button" -> {
                    items?.add(RowType.ButtonRowType)
                    buttonCount++
                }
                "text" -> {
                    items?.add(RowType.TextRowType())
                    textEditCount++
                }
                else -> Utils.showToast(this, "Text isn't correct")
            }
        }
        recyclerView.adapter?.notifyDataSetChanged()
        if (buttonCount > 0) {
            Utils.addToLog("Number of buttons added $buttonCount")
        }
        if (textEditCount > 0){
            Utils.addToLog("Number of textEdit added $textEditCount")
        }
    }
}
