package com.example.homework1

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment


class AddElemDialogFragment : DialogFragment() {

    private var addElemDialogListener : AddElemDialogListener? = null
    private var editTextView: EditText? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = arguments?.getString("title")
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.dialog_fragment, null)
        editTextView = view.findViewById(R.id.editTextInDialogFragment) as EditText
        builder.setTitle(title)
        builder.setPositiveButton("OK") { dialog, id ->
            addElemDialogListener?.onDialogPositiveClick(
                this,
                editTextView!!.text.toString()
            )
        }
        builder.setView(view)
        return builder.create()
    }

    interface AddElemDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment, text: String)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            addElemDialogListener = activity as AddElemDialogListener
        }
        catch (e: ClassCastException) {
            throw ClassCastException(("$activity must implement NoticeDialogListener"))
        }
    }

    companion object{
        fun newInstance() = AddElemDialogFragment().apply {
            val title = "Add elements"
            val args = Bundle()
            args.putString("title", title)
            arguments = args
        }
    }
}